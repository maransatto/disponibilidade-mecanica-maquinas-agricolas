# Sistema integrado de exibição de disponibilidade mecânica de máquinas agrícolas

## Descrição

Este projeto é um exemplo funcional de um sistema que demonstra um relatório de disponibilidade mecânica de máquinas agrícolas de uma propriedade. Apenas o conceito, sem controle complexo de acesso, clientes, etc.

## Objetivos

1. Permite efetuar cadastro de modelos de máquinas
2. Permite efetuar cadastro de máquinas
3. Demonstra relatório de disponibilidade da máquina com os dados retornados pela API da REXAGRI em conjunto com os dados complementares cadastrados neste sistema, com informações das máquinas e modelo.

## Recursos utilizados

Mysql para o banco de dados, Node.JS sem framework específico para REST API, Angular com bootstrap para aplicativo web.

### Observações

O objetivo pelo qual não usei um framework específico para API (ex: Loopback) é que achei mais fácil construir do zero e isso também me dá uma liberdade melhor de customização.

## Atividades

Para controle das atividades de criação deste projeto usei [um board no Trello](https://trello.com/b/oumEil4Z/enalta-disponibilidade-mecânica)